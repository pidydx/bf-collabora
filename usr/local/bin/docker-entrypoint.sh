#!/bin/bash

set -e

if [ "$1" = 'collabora' ]; then
    if [ ! -f /etc/coolwsd/keys/proof_key ]; then
        ssh-keygen -t rsa -N "" -m PEM -f /etc/coolwsd/keys/proof_key
    fi
    exec coolwsd
fi

exec "$@"
