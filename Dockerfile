#########################
# Create base container #
#########################
FROM ubuntu:24.04 as base
LABEL maintainer="pidydx"

# Base setup
ENV APP_USER=cool
ENV APP_GROUP=cool

ENV COLLABORA_VERSION=24.04

# Update users and groups
ENV BASE_PKGS code-brand=${COLLABORA_VERSION}-* collaboraoffice-dict-en collaboraofficebasis-python-script-provider \
    collaboraofficebasis-pyuno coolwsd-deprecated fonts-open-sans fonts-droid-fallback fonts-noto-cjk openssh-client

# Create app user and group
RUN userdel ubuntu
RUN groupadd -g 999 ${APP_GROUP} \
 && useradd -r -M -N -u 999 ${APP_USER} -g ${APP_GROUP} -s /usr/sbin/nologin


 # Update and install base packages
COPY etc/apt /etc/apt

RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ca-certificates \
 && apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BASE_PKGS} \
 && rm -rf /var/lib/apt/lists/*


##########################
# Create final container #
##########################
FROM base

# Prepare container
COPY etc/ /etc/
COPY usr/ /usr/

RUN mkdir -p /var/lib/cool/jails \
 && chown -R ${APP_USER}:${APP_GROUP} /var/lib/cool \
 && mkdir -p /etc/coolwsd/keys \
 && rm /etc/coolwsd/proof_key \
 && rm /etc/coolwsd/proof_key.pub \
 && ln -s /etc/coolwsd/keys/proof_key /etc/coolwsd/proof_key \
 && ln -s /etc/coolwsd/keys/proof_key.pub /etc/coolwsd/proof_key.pub \
 && chown -R ${APP_USER}:${APP_GROUP} /etc/coolwsd \
 && ln -s /etc/ssl/certs/ca-certificates.crt /etc/coolwsd/ca-chain.cert.pem \
 && rm /opt/cool/systemplate/etc/resolv.conf \
 && ln -s /etc/resolv.conf /opt/cool/systemplate/etc/resolv.conf \
 && rm /opt/cool/systemplate/etc/hosts \
 && ln -s /etc/hosts /opt/cool/systemplate/etc/hosts

EXPOSE 9980/tcp
VOLUME ["/var/lib/cool"]

USER $APP_USER
ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["collabora"]
